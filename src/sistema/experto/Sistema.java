/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.experto;

import Rule.*;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuGFA
 */
public final class Sistema {

    BooleanRuleBase BaseReglas;
    ArrayList<RuleVariable> variables;
    
    public Sistema(String base)
    {
        this.CargarBase(base);
    }
    
    public Sistema()
    {
        this.BaseReglas = new BooleanRuleBase("Reglas");
    }
    
    public String Result() throws IOException
    {
        BaseReglas.forwardChain();
        return variables.get(variables.size() - 1).getValue();
    }
    
    public void CargarBase(String base)
    {
        this.BaseReglas = new BooleanRuleBase("Reglas");
        
        DataInputStream datos = null;
        try
        {
            datos = Input.readOpen(base);
            String line;
            variables = new ArrayList();
            while(!(line = datos.readLine()).trim().isEmpty())
            {
                if(line.contains("#"))
                {
                    String var = line.replace("#", "");
                    variables.add(new RuleVariable(BaseReglas, var));
                }
            }   while(!"END".equals((line = datos.readLine()).trim()))
            {
                if(line.isEmpty())
                {
                    continue;
                }
                
                line = line.trim();
                if(line.contains(":"))
                {
                    String title = line.replace(":", "");
                    ArrayList<Clause> ruleClauses = new ArrayList();
                    ArrayList<Clause> ruleClauses1 = new ArrayList();
                    
                    while(!(line = datos.readLine()).trim().contains("THEN"))
                    {
                        String[] ln = line.split(" ");
                        ruleClauses.add(new Clause(BaseReglas.getVariable(ln[0].trim()), new Condition(ln[1].trim()), ln[2].trim()));
                        ruleClauses1.add(new Clause(BaseReglas.getVariable(ln[0].trim()), new Condition(ln[1].trim()), ln[2].trim()));
                    }
                    
                    line = line.replace("THEN", "").trim();
                    String[] ln = line.split(" ");
                    
                    Clause[] clauseArray = new Clause[ruleClauses.size()];
                    Clause[] clauseArray1 = new Clause[ruleClauses.size()];
                    
                    for(int i = 0; i < ruleClauses.size(); i++)
                    {
                        clauseArray[i] = ruleClauses.get(i);
                        clauseArray1[i] = ruleClauses1.get(i);
                    }
                    
                    Clause goalClause = new Clause(BaseReglas.getVariable(ln[0].trim()), new Condition(ln[1].trim()), ln[2].trim());
                    Clause goalClause1 = new Clause(BaseReglas.getVariable(ln[0].trim()), new Condition(ln[1].trim()), ln[2].trim());
                    
                    new Rule(
                        BaseReglas,
                        title,
                        clauseArray,
                        goalClause
                    );
                    
                    int i = 1;
                    for(Clause clause : clauseArray1)
                    { 
                        new Rule(
                            BaseReglas,
                            title + i,
                            goalClause1,
                            clause
                        );
                        i++;
                    }
                }
            }
        }
        catch (IOException ex)
        {
            System.out.println("Error con la base de conocimiento.");
        }
        finally
        {
            try
            {
                datos.close();
            } catch (IOException ex)
            {
                
            }
        }
    }
}

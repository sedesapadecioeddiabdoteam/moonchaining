package sistema.experto;

import java.io.*;

public class Input
{
    public static DataInputStream readOpen(String a) throws FileNotFoundException
    {
        return new DataInputStream(new FileInputStream(a));
    }

    public static DataOutputStream writeOpen(String a) throws FileNotFoundException
    {
        return new DataOutputStream(new FileOutputStream(a));
    }

    public static DataOutputStream addOpen(String a) throws FileNotFoundException
    {
        return new DataOutputStream(new FileOutputStream(a, true));
    }
}